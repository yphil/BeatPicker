#!/usr/bin/env bash

VERSION=$(head -1 debian/changelog | grep -oP "(?<=\().*(?=\))")

if debuild -I -us -uc; then

    read -e -p "
#### Install Beatpicker (beatpicker_${VERSION}_all.deb)? [Y/n] " YN

    if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
        sudo apt install gdebi
        sudo gdebi -n ../beatpicker_${VERSION}_all.deb
    fi

    read -e -p "
#### Upload Beatpicker binaries to bitbucket repo download section? [y/N] " YN

    if [[ $YN == "y" || $YN == "Y" ]] ; then
        curl --progress-bar -o upload.log -u yassinphilip -X POST https://api.bitbucket.org/2.0/repositories/yassinphilip/beatpicker/downloads --progress-bar -F files=@../beatpicker_${VERSION}_all.deb -F files=@../beatpicker_${VERSION}.tar.gz && rm -f upload.log

    fi

    read -e -p "
#### Cleanup Beatpicker package files? [Y/n] " YN
    if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
        rm -fv ../beatpicker_${VERSION}*
    fi

fi
